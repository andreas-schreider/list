package andreas.schreider.shoppinglist;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final Integer RecordAudioRequestCode = 1;
    private SpeechRecognizer speechRecognizer;
    static ArrayList<String> shoppingList = new ArrayList<String>();
    static ArrayAdapter arrayAdapter;
    SQLiteDatabase myDatabase;
    ListView lvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //TEXT TO SPEECH
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            checkPermission();
        }
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        final Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {


            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                myDatabase = openOrCreateDatabase("Articles", MODE_PRIVATE, null);
                myDatabase.execSQL("CREATE TABLE IF NOT EXISTS articles (name VARCHAR, amount INT(5), id PRIMARY KEY)");
                myDatabase.execSQL("INSERT INTO articles (name, amount) VALUES ('" + data.get(0) + "', 4)");
                shoppingList.add(data.get(0));
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


        //LIST
        lvList = findViewById(R.id.lvList);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, shoppingList);

        lvList.setAdapter(arrayAdapter);
        try {
            myDatabase = this.openOrCreateDatabase("Articles", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS articles (name VARCHAR, amount INT(5), id PRIMARY KEY)");
            Cursor c = myDatabase.rawQuery("SELECT * FROM articles ", null);

            int nameIndex = c.getColumnIndex("name");
            int amountIndex = c.getColumnIndex("amount");
            int idIndex = c.getColumnIndex("id");


            if (c.moveToFirst()) {
                do {
                    shoppingList.add(c.getString(nameIndex));
                    arrayAdapter.notifyDataSetChanged();

                } while (c.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        ;

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("noteId", i);
                startActivity(intent);
            }
        });

        lvList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String listItem = shoppingList.get(i);
                try {

                    myDatabase.execSQL("CREATE TABLE IF NOT EXISTS articles (name VARCHAR, amount INT(5), id PRIMARY KEY)");
                    myDatabase.execSQL("DELETE FROM articles WHERE articles.name LIKE '" + shoppingList.get(i).toString() + "'");

                    shoppingList.remove(i);
                    arrayAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
                arrayAdapter.notifyDataSetChanged();
                return true;
            }
        });


        //New Item
        FloatingActionButton btSpeech = findViewById(R.id.btSpeech);
        btSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speechRecognizer.startListening(speechRecognizerIntent);


            }
        });


        FloatingActionButton fab = findViewById(R.id.btText);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder adInput = new AlertDialog.Builder(MainActivity.this);
                adInput.setTitle("Bezeichnung");

                final EditText etObject = new EditText(MainActivity.this);
                etObject.setInputType(InputType.TYPE_CLASS_TEXT);
                adInput.setView(etObject);

                adInput.setPositiveButton("hinzufügen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        myDatabase = openOrCreateDatabase("Articles", MODE_PRIVATE, null);
                        myDatabase.execSQL("CREATE TABLE IF NOT EXISTS articles (name VARCHAR, amount INT(5), id PRIMARY KEY)");
                        myDatabase.execSQL("INSERT INTO articles (name, amount) VALUES ('" + etObject.getText().toString() + "', 4)");
                        shoppingList.add(etObject.getText().toString());
                        arrayAdapter.notifyDataSetChanged();
                    }
                });

                adInput.setNegativeButton("abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                adInput.show();



            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            speechRecognizer.stopListening();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        speechRecognizer.destroy();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, RecordAudioRequestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RecordAudioRequestCode && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
        }
    }
}